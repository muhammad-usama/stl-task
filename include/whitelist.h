#ifndef INCLUDE_WHITELIST_H
#define INCLUDE_WHITELIST_H

#include <list>
#include "student.h"
class whitelist
{
private:
  std::map<std::string, student *> student_record;
  std::list<student> student_data_list;
  std::map<std::string, student *>::iterator student_iter;

public:
  void add_to_whitelist(std::string, student);
  bool is_student_present(std::string);
  student *get_student_data();
};
#endif