#ifndef INCLUDE_STUDENT_H
#define INCLUDE_STUDENT_H
#include <string>
#include <map>

class student
{
private:
    struct student_record
    {
        std::string name;
        std::string roll_no;
        uint8_t age;
        float cgpa;
    } student_info;
    std::map<std::string /*subject name*/, uint8_t /*marks*/> result;

public:
    student();
    student(const std::string &name, const std::string &roll_no, uint8_t age, float cgpa);
    ~student();
    const int8_t get_subject_marks(const std::string &subject);
    void set_subject_marks(const std::string &subject, uint8_t marks);
    void print_all_marks();
    void print_student_data();
    // Setter Functions
    void set_name(const std::string &name);
    void set_rollno(const std::string &roll_no);
    void set_age(uint8_t age);
    void set_cgpa(float cgpa);
    // Getter Functions
    const std::string &get_name() const;
    const std::string &get_rollno() const;
    const uint8_t &get_age() const;
    const float &get_cgpa() const;
};

#endif