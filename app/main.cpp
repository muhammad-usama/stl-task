#include <iostream>
#include "whitelist.h"
using namespace std;

int main()
{
    student s1("Student 1", 21, 3.11);
    student s2("Student 2", 22, 3.12);
    student s3("Student 3", 23, 3.43);
    student s4("Student 4", 21, 3.72);
    student s5("Student 5", 22, 2.90);
    /*
    student s6("Student 6", 23, 2.10);
    student s7("Student 7", 21, 1.00);
    student s8("Student 8", 22, 2.50);
    student s9("Student 9", 23, 3.90);
    student s10("Student 10", 21, 2.75);
    student s11("Student 11", 22, 3.33);
    student s12("Student 12", 23, 3.67);
    student s13("Student 13", 21, 2.55);
    student s14("Student 14", 22, 3.14);
    student s15("Student 15", 23, 3.50);
    */
    whitelist w;
    w.add_to_whitelist("s1",s1);
    w.add_to_whitelist("s2",s2);
    w.add_to_whitelist("s3",s3);
    w.add_to_whitelist("s4",s4);
    w.add_to_whitelist("s5",s5);
    w.add_to_whitelist("s1",s1);
}