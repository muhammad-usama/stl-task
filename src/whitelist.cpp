#include <iostream>
#include "whitelist.h"
using namespace std;

bool whitelist::is_student_present(string student_name)
{
    student_iter = student_record.find(student_name);
    if(student_iter != student_record.end())
        return true; 
    else
        return false;
}

void whitelist::add_to_whitelist(string name, student student_data)
{
    if(is_student_present(name) == true)
    {
        char option;
        cout << "Student name already present in the record!\nPress 'U' to update record: ";
        cin >> option;
        if(option == 'U' || option == 'u')
        {
            *(student_iter->second) = student_data;
            cout << "Student data successfully updated in the record!" << endl;
        }     
    }
    else
    {
        student_data_list.push_back(student_data);
        student_record[name] = &student_data_list.back();
        cout << "Student data successfully added in the record!" << endl;
    }
}

student* whitelist::get_student_data() 
{
    return student_iter->second;
}