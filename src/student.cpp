#include <iostream>
#include "student.h"

using namespace std;

student::student()
{
    //  cout << "Default constructor is called." << endl;
    student_info.name = "NULL";
    student_info.roll_no = "NULL";
    student_info.age = 5;
    student_info.cgpa = 0.0;
}

student::student(const string &name, const string &roll_no, uint8_t age, float cgpa)
{
    //  cout << "Parameterized constructor is called." << endl;
    set_name(name);
    set_rollno(roll_no);
    set_age(age);
    set_cgpa(cgpa);
}

const int8_t student::get_subject_marks(const string &subject)
{
    if (result.find(subject) != result.end())
        return result[subject];
    else
    {
        cout << "The subject '" << subject << "' doesn't exist in the record!" << endl;
        return -1;
    }
}

void student::set_subject_marks(const string &subject, uint8_t marks)
{
    while (true)
    {
        if (marks >= 0 && marks <= 100)
        {
            if (result.find(subject) == result.end())
                cout << "Marks set successfully." << endl;
            else
                cout << "Marks updated successfully." << endl;
            result[subject] = marks;
            return;
        }
        else
        {
            cout << "You have entered invalid marks for subject " << subject << ".\nEnter marks greater than or equal to 0: ";
            cin >> marks;
        }
    }
}

void student::print_all_marks()
{
    map<string, uint8_t>::iterator iter;
    cout << "----------------------------------------------------" << endl;
    cout << "Subject\tMarks" << endl;
    for (iter = result.begin(); iter != result.end(); iter++)
    {
        cout << (*iter).first << "\t" << (*iter).second << endl;
    }
    cout << "----------------------------------------------------" << endl;
}

void student::set_age(uint8_t age)
{
    while (true)
    {
        if (age >= 5)
        {
            student_info.age = age;
            //    cout << "Age set successfully." << endl;
            return;
        }
        else
        {
            cout << "Invalid age.\nEnter age greater than or equal to 5:";
            cin >> age;
        }
    }
}

void student::set_cgpa(float cgpa)
{
    while (true)
    {
        if (cgpa >= 0 && cgpa <= 4)
        {
            student_info.cgpa = cgpa;
            //   cout << "CGPA set successfully." << endl;
            return;
        }
        else
        {
            cout << "Invalid cgpa.\nEnter cgpa between 0 - 4:";
            cin >> cgpa;
        }
    }
}

void student::set_rollno(const std::string &roll_no)
{
    student_info.roll_no = roll_no;
}

void student::set_name(const std::string &name)
{
    student_info.name = name;
}

const string &student::get_name() const
{
    return student_info.name;
}

const string &student::get_rollno() const
{
    return student_info.roll_no;
}

const uint8_t &student::get_age() const
{
    return student_info.age;
}

const float &student::get_cgpa() const
{
    return student_info.cgpa;
}

void student::print_student_data()
{
    cout << "----------------------------------------------------" << endl;
    cout << "Name: " << get_name() << endl;
    cout << "Roll No: " << get_rollno() << endl;
    cout << "CGPA: " << get_cgpa() << endl;
    cout << "Age: " << get_age() << endl;
    print_all_marks();
}
student::~student()
{
    // cout << "Destructor is called." << endl;
}
